# README #

Este repositório contém todos os arquivos necessários para implantação e utilização da aplicação **Tweeps-Locaweb**.

### Aplicação ###

Esta aplicação tem como objetivo oferecer, em formato JSON, consultas a tweets que mencionaram a Locaweb em dois agrupamentos: tweets mais relevantes e usuários que mais fizeram menções. Ambos possuem regras de ordenação, aplicadas cada uma à sua forma em cada agrupamento, definidas por nº de seguires, nº de retweets e nº de likes.

O resultado oferece tweets com os campos definidos pelo escopo. Eles são: ```screen_name, followers, retweets, likes, text, created_at``` e ```link ```

### Tecnologia ###

O Tweeps-Locaweb foi criado e desenvolvido utilizando virtualização de máquinas com **Vagrant**, em uma box com **Ubuntu**, programação em **PHP 7.1** com o framework **Silex 2** e testes unitários com **PHPUnit**.

O [PHP](http://php.net/) foi escolhido por sua rápida performance, por ser uma das melhores soluções web no mercado e pela quantidade de recursos disponíveis para lidar com pacotes JSON e sistemas de APIs, tanto no fornecimento como no recebimento de dados. Para utilizar o máximo de seus recursos, a versão 7.1 foi usada.

Portanto, foi natural escolher o micro-framework [Silex](http://silex.sensiolabs.org/) para trabalhar, pois, além de usar os recursos naturais do PHP, ele foi criado justamente para prover soluções para micro-aplicações. Ela trabalha naturalmente com interfaces JSON e mostra ao usuário resultados bem claros e organizados, com os códigos de retorno ```HTTP``` corretos e pacotes corretamente formatados.

Para desenvolvimento, foi optado por uma máquina [Vagrant](https://www.vagrantup.com/) com [Ubuntu](https://www.ubuntu.com/), o que permitiu virtualizar uma máquina relativamente próxima do que se tem hoje no mercado de hospedagens de sites e aplicações. Com as características que possui, a Box usada se parece com um servidor de desenvolvimento ou de homologação de aplicações.

A fim de versionar o desenvolvimento da aplicação, foi utilizado o ```git flow```.

Por fim, [PHPUnit](https://phpunit.de/) foi a tecnologia selecionada para fazer testes automatizados por oferecer diversos recursos com uma ótima e [extensa documentação](https://phpunit.de/manual/current/pt_br/index.html).

### Instalação & Configuração ###

Para utilizar o repositório, desenvolver e testar a aplicação siga os seguintes passos na ordem:

- Certifique-se de que você possui o ```Vagrant``` instalado na máquina. Se não tiver, [baixe aqui](https://www.vagrantup.com/downloads.html)
- Baixe o ```arquivo zip``` e descompacte-o em um diretório
- Verifique se os arquivos ```Vagrantfile``` e ```provision.sh``` estão lá
- O ```Vagrantfile``` contém as configurações necessárias para instalar a Box; o ```provision.sh``` é um arquivo [shell script](https://pt.wikipedia.org/wiki/Shell_script) que instala e configura o que é necessário para a máquina virtual rodar o mínimo necessário para a aplicação
- Inicie a máquina virtual executando o comando ```vagrant up``` no terminal, no diretório em que você descompactou o arquivo
- Este comando iniciará a máquina virtual e instalará nela tudo que foi definido no arquivo de provisões: PHP 7.1, Apache2, bibliotecas adicionais e configura o servidor para aceitar ```pretty URLs```
- Se houver algum problema no processo de instalação, repetir o ```vagrant up``` ou usar ```vagrant provision``` é o mais recomendado
- Depois que a máquina for toda instalada (e configurada), execute ```vagrant ssh``` no terminal. Você passará a navegar dentro da Box virtualizada. Não se preocupe, é como usar um terminal no seu próprio computador ;)
- Vá até o diretório ```/var/www/html```, onde o projeto estará por padrão, para instalar os pacotes PHP necessários para a aplicação. Isso acontece com o ```composer```, a tecnologia do PHP que instala as dependências necessárias automaticamente de acordo com as pré-definições colocadas em seu arquivo ```composer.json```. O arquivo configurado já faz parte deste pacote
- Portanto, instale as dependências com ```composer install```. Não há necessidade de popular bases de dados - a API não possui banco e está pronta para ser usada.
- Pronto! Você já pode usar o **Tweeps-Locaweb**! Mas como fazê-lo?

#### Utilização - Aplicação ####

- A máquina virtual configura uma porta padrão para testar a aplicação,o a 8080. Portanto, para acessá-la deve-se utilizar ```http://localhost:8080```
- ```Endpoint /most_relevants```: É neste endpoint que poderá se obter os tweets mais relevantes seguindo a regra de ordenação definida pelo escopo da aplicação
- ```Endpoint /most_mentions```: É neste endpoint que poderá se obter os tweets mais relevantes seguindo a regra de ordenação definida pelo escopo da aplicação agrupados pelos usuários que mais mencionaram a Locaweb

Uma ferramenta recomendada para fazer os testes é o [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop), extensão do Google Chrome para acessar APIs com todas as possibilidades que o protocolo HTTP oferece.

#### Utilização - Testes ####

- A máquina virtual já instala o PHPUnit por padrão
- Para executar testes automatizados da aplicação, certifique-se de estar em um terminal no diretório raiz dela
- Execute o comando ```phpunit``` e verifique os resultados. Se tudo certo, você terá a execução correta de 9 testes com 25 asserções

Importante: as pré-definições para os testes estão no arquivo ```phpunit.xml.dist``` que está na raiz. Ele não deve ser altado.