<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app->error(function (Exception $e) use ($app) {
	$httpStatusCode = 500;
	if ($e instanceof MethodNotAllowedHttpException) {
		$data['message'] = 'HTTP method not allowed.';
		$httpStatusCode = $e->getStatusCode();
	}
	
	if ($e instanceof NotFoundHttpException) {
		$data['message'] = 'Resource not found.';
		$httpStatusCode = 404;
	}
	
	if (! isset($data['message'])) {
		if ($app['env'] == 'dev') {
			$data['message'] = $e->getMessage();
			$data['file'] = $e->getFile();
			$data['line'] = $e->getLine();
		} else {
			$data['message'] = 'Internal Error.';
		}
	}
	
	return new JsonResponse($data, $httpStatusCode);
});