<?php
namespace Tweeps\Controllers;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class MostRelevantController implements ControllerProviderInterface
{
	
	const STATUS_OK = 200;
	
	public function connect(Application $app)
	{
		$service = new \Tweeps\Services\TweepsService();
		if ($service->extractData() == self::STATUS_OK) {
			
			$controller = $app['controllers_factory'];
			
			$controller->get('/', function(Application $app) use ($service) {
				$data = $service->filterMostRelevant();
				return new JsonResponse($data, 200);
			});
				
			return $controller;
				
		} else {
			
			return new JsonResponse('Bad Request', 400);
			
		}
		
	}
	
}
