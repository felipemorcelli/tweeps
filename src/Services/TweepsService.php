<?php
namespace Tweeps\Services;

class TweepsService implements \Tweeps\Interfaces\TweepsInterface {
	
	const STATUS_OK = 200;
	
	private $iniFile;
	
	public $result;
	
	/**
	 * extractData - method to extract tweets from the API
	 * @param null
	 * @return int http_code
	 */
	public function extractData() {

		// obtain user from ini file
		$this->checkIniFile(__DIR__ . '/../../setup/header.ini');
		$this->iniFile = parse_ini_file(__DIR__ . '/../../setup/header.ini');

		$curlHandler = curl_init($this->iniFile['Endpoint']);
		curl_setopt($curlHandler, CURLOPT_POST, false);
		curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandler, CURLOPT_HTTPHEADER, [
				'Username: ' . $this->iniFile['Username']
		]);
		
		$result = json_decode(curl_exec($curlHandler), true);
		$httpCode = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
		
		if ($httpCode == self::STATUS_OK)
			$this->result = $result;
			
		return $httpCode;
			
	}
	
	/**
	 * filterMostMentions - method to filter tweets with highest mentions quantity
	 * @param null
	 * @return array
	 */
	public function filterMostMentions() {
		$data = $this->result['statuses'];
		$newData = [];
		
		foreach ($data as $row) {
			if (isset($row['entities']['user_mentions'][0])) {
				$user = $row['entities']['user_mentions'][0];
				if ($user['id'] == $this->iniFile['LocawebUser'] && $row['in_reply_to_user_id'] != $this->iniFile['LocawebUser']) {
					
					if (!isset($newData[$row['user']['screen_name']])) $newData[$row['user']['screen_name']] = [];

					array_push($newData[$row['user']['screen_name']], [
						'screen_name' => $row['user']['screen_name'],
						'followers' => $row['user']['followers_count'],
						'retweets' => $row['retweet_count'],
						'likes' => $row['favorite_count'],
						'text' => $row['text'],
						'created_at' => $row['created_at'],
						'link' => "https://twitter.com/{$row['user']['screen_name']}/status/{$row['id']}",
					]);
					
				}
			}
		}
		
		$newData = $this->orderData($newData, 2);
		
		return $newData;
	}
	
	/**
	 * filterMostMentions - method to filter most relevant
	 * @param null
	 * @return array
	 */
	public function filterMostRelevant() {
		$data = $this->result['statuses'];
		$newData = [];
		
		foreach ($data as $row) {
			if (isset($row['entities']['user_mentions'][0])) {
				$user = $row['entities']['user_mentions'][0];
				if ($user['id'] == $this->iniFile['LocawebUser'] && $row['in_reply_to_user_id'] != $this->iniFile['LocawebUser']) {
					$newData[] = [
						'screen_name' => $row['user']['screen_name'],
						'followers' => $row['user']['followers_count'],
						'retweets' => $row['retweet_count'],
						'likes' => $row['favorite_count'],
						'text' => $row['text'],
						'created_at' => $row['created_at'],
						'link' => "https://twitter.com/{$row['user']['screen_name']}/status/{$row['id']}",
					];
				}
			}
		}
		
		$newData = $this->orderData($newData, 1);
		
		return $newData;
	}
	
	/**
	 * orderData - method to correctly order the data
	 * @param array $newData, int $type
	 * @return array
	 */
	public function orderData(array $newData, int $type = 1) {
		$sort = $newData;
		
		if (is_null($newData) || count($newData) == 0) throw new \Exception('Invalid data');
		
		if ($type == 1) {
			foreach ($sort as $key => $row) {
				$followers[$key]  = $row['followers'];
				$retweets[$key] = $row['retweets'];
				$likes[$key] = $row['likes'];
			}
			
			array_multisort($followers, SORT_DESC, $retweets, SORT_DESC, $likes, SORT_DESC, $sort);
			
		} else if ($type == 2) {
			uasort($sort, function($a, $b) {
				return (count($b) - count($a));
			});
			
			$newSort = [];
			
			foreach ($sort as $user) {
				$index = $user[0]['screen_name'];
				foreach ($user as $key => $row) {
					$followers[$key]  = $row['followers'];
					$retweets[$key] = $row['retweets'];
					$likes[$key] = $row['likes'];
				}
				
				array_multisort($followers, SORT_DESC, $retweets, SORT_DESC, $likes, SORT_DESC, $user);
				
				$newSort[$index] = $user;
			}

			$sort = $newSort;
			
		} else {
			throw new \Exception('Invalid type format');
		}
		
		return $sort;
	}
	
	/**
	 * checkIniFile - method to check if INI file exists
	 * @param null
	 * @return string $username
	 */
	public function checkIniFile($location) {
		if (!file_exists($location))
			throw new \Exception('INI file not found');
	}
	
}
