<?php
/**
 * @covers TweepsService
 */
use Silex\Application;

final class MostMentionsControllerTest extends \PHPUnit_Framework_TestCase
{
	
	private $controller;
	
	public function setUp() {
		$this->controller = new \Tweeps\Controllers\MostMentionsController();
	}
	
	public function tearDown() {
		unset($this->controller);
	}
	
	public function testConnection() {
		$app = new Application();
		$res = $this->controller->connect($app);
		
		$this->assertTrue(is_a($res, 'Silex\ControllerCollection'));
	}
	
}