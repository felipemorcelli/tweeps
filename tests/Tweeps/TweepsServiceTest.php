<?php
/**
 * @covers TweepsService
 */
final class TweepServiceTest extends \PHPUnit_Framework_TestCase
{
	
	private $service;
	
	private $http_code;
	
	public function setUp() {
		$this->service = new \Tweeps\Services\TweepsService();
		$this->http_code = $this->service->extractData();
	}
	
	public function tearDown() {
		unset($this->service);
	}
	
	public function testExtractData() {
		$this->assertEquals(200, $this->http_code);
	}
	
	public function testDataIsJSONFormat() {
		$json = json_encode($this->service->result);
		$this->assertEquals(0, json_last_error());
	}
	
	public function testDataIsBiggerThanZero() {
		$this->assertTrue(count($this->service->result) > 0);
	}
	
	public function testFilterAnyMentionFormat() {
		$structure = ['screen_name', 'followers', 'retweets', 'likes', 'text', 'created_at', 'link'];
		$data = $this->service->filterMostMentions();
		foreach ($data as $row) {
			$compare = $row;
			break;
		}

		for ($i=0; $i < count($structure); $i++) {
			$this->assertArrayHasKey($structure[$i], $row[0]);
		}
		
		$data = $this->service->filterMostRelevant();
		foreach ($data as $row) {
			$compare = $row;
			break;
		}
		
		for ($i=0; $i < count($structure); $i++) {
			$this->assertArrayHasKey($structure[$i], $row);
		}
		
	}
	
	/**
	 * @expectedException Exception
	 * @expectedExceptionMessage Invalid data
	 */
	public function testExceptionWhenDataIsInvalid() {
		$this->service->orderData(array(), 1);
	}
	
	/**
	 * @expectedException Exception
	 * @expectedExceptionMessage Invalid type format
	 */
	public function testExceptionWhenTypeFormatIsInvalid() {
		$this->service->orderData(['key' => 'value'], 3);
	}
	
	/**
	 * @expectedException Exception
	 * @expectedExceptionMessage INI file not found
	 */
	public function testExceptionWhenIniFileNotExists() {
		$this->service->checkIniFile('no location');
	}
	
}