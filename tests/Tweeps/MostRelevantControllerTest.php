<?php
/**
 * @covers TweepsService
 */
use Silex\Application;

final class MostRelevantControllerTest extends \PHPUnit_Framework_TestCase
{

	private $controller;
	
	public function setUp() {
		$this->controller = new \Tweeps\Controllers\MostRelevantController();
	}
	
	public function tearDown() {
		unset($this->controller);
	}
	
	public function testConnection() {
		$app = new Application();
		$res = $this->controller->connect($app);
		
		$this->assertTrue(is_a($res, 'Silex\ControllerCollection'));
	}

}