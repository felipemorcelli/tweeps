<?php
require_once __DIR__ . '/vendor/autoload.php';

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

$app = new Application();

$app->get('/', function () {
	return new JsonResponse('Welcome to Locaweb\'s Tweeps!', 200);
});
	
$app->mount('/most_mentions', new Tweeps\Controllers\MostMentionsController());
$app->mount('/most_relevants', new Tweeps\Controllers\MostRelevantController());
	
$app->run();
	
return $app;
	